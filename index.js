// App Logic
$(document).ready(function() {
    if (window.localStorage.getItem('Contacts') == null) {
        Contacts = [
            {"id":1, "firstName":"Almog","lastName":"Levy","phoneNumber":"0523184433","email":"almoglevy26@gmail.com"},
            {"id":2, "firstName":"Gal","lastName":"Levy","phoneNumber":"0543124111","email":"gallevy10@gmail.com"}
        ]
        window.localStorage.setItem('Contacts', JSON.stringify(Contacts));
    }

    Contacts = JSON.parse(window.localStorage.getItem('Contacts'));
    for (let key in Contacts) {
        let obj = Contacts[key];
        let tableRow = '<tr id="contact'+ obj.id + '">\n' +
            '<td class=fName contenteditable="true">'+ obj.firstName + '</td>\n' +
            '<td class=lName contenteditable="true">'+ obj.lastName + '</td>\n' +
            '<td class=pNum contenteditable="true">'+ obj.phoneNumber + '</td>\n' +
            '<td class=email contenteditable="true">'+ obj.email + '</td>\n' +
            '<td><button onclick=editContact('+ obj.id +') id="editContact' + obj.id +'"><i class="fa-solid fa-pen"></i></button></td>\n' +
            '<td><button onclick=deleteContact('+ obj.id +') id="deleteContact' + obj.id +'"><i class="fa-solid fa-trash-can"></i></button></td>\n' +
            'tr'
        $('#contacts').append(tableRow);
    }
});

function deleteContact(id) {
    contacts = JSON.parse(window.localStorage.getItem('Contacts'));
    let index = 0;
    let i = 0;
    for (contact in contacts) {
        if(contact.id == id) {
            index = i;
        }
        i++;
    }
    contacts.splice(index-1, 1);
    window.localStorage.setItem('Contacts', JSON.stringify(contacts));
    $('table#contacts tr#contact' + id).remove();
    $('table#contacts tr#search-contact' + id).remove();
}

function clean_inputs(){
    $('input#fName').val('');
    $('input#lName').val('');
    $('input#pNum').val('');
    $('input#email').val('');
    $('input#fname-search').val('');
}

function editContact(id) {
    let contacts = JSON.parse(window.localStorage.getItem('Contacts'));
    let  isSearchOpen = $('#search-contact table#contacts tr#search-contact' + id).length > 0;
    
    let contactsFnameTd =  $('table#contacts tr#contact' + id + ' td.fName').html();
    let searchContactsFnameTd =  isSearchOpen ?  $('table#contacts tr#search-contact' + id + ' td.fName').html() : null;

    let contactslNameTd =  $('table#contacts tr#contact' + id + ' td.lName').html();
    let searchContactslNameTd =  isSearchOpen ?   $('table#contacts tr#search-contact' + id + ' td.lName').html() : null;

    let contactsPnumTd =  $('table#contacts tr#contact' + id + ' td.pNum').html();
    let searchContactspNumTd =  isSearchOpen ?  $('table#contacts tr#search-contact' + id + ' td.pNum').html() : null;

    let contactsEmailTd =  $('table#contacts tr#contact' + id + ' td.email').html();
    let searchContactsEmailTd =  isSearchOpen ?  $('table#contacts tr#search-contact' + id + ' td.email').html() : null;

    let fName =  (searchContactsFnameTd != null) ? searchContactsFnameTd : contactsFnameTd;
    let lName =  (searchContactslNameTd != null) ? searchContactslNameTd : contactslNameTd;
    let pNum =  (searchContactspNumTd != null) ? searchContactspNumTd : contactsPnumTd;
    let email =  (searchContactsEmailTd != null) ? searchContactsEmailTd : contactsEmailTd;

    if(fName) { contacts[id-1].firstName = fName;}
    if(lName) { contacts[id-1].lastName = lName;}
    if(pNum) {contacts[id-1].phoneNumber = pNum;}
    if(email) { contacts[id-1].email = email;}

    if (isSearchOpen) {
        if(searchContactsFnameTd) {
            $('#search-contact table#contacts tr#search-contact' + id + 'td .fName').html(fName); //its a nightmare - prefer to use REACT!
        }
        if(searchContactslNameTd) {
            $('#search-contact table#contacts tr#search-contact' + id + 'td .lName').html(lName); //its a nightmare - prefer to use REACT!
        }
        if(searchContactspNumTd) {
            $('#search-contact table#contacts tr#search-contact' + id + 'td .pNum').html(pNum); //its a nightmare - prefer to use REACT!
        }
        if(searchContactsEmailTd) {
            $('#search-contact table#contacts tr#search-contact' + id + 'td .email').html(email); //its a nightmare - prefer to use REACT!
        }
    }
    window.localStorage.removeItem('Contacts');
    window.localStorage.setItem('Contacts', JSON.stringify(contacts));
}

function addContact(){
    let contacts = JSON.parse(window.localStorage.getItem('Contacts'));
    let firstName = $('.contact-row #fName').val();
    let lastName = $('.contact-row #lName').val();
    let phoneNumber = $('.contact-row #pNum').val();
    let email = $('.contact-row #email').val();

    clean_inputs();

    if (firstName != '' && lastName != '' && phoneNumber != '' && email != '') {
        let newContact = {
            "id": contacts[contacts.length-1].id + 1,
            "firstName": firstName,
            "lastName":lastName,
            "phoneNumber": phoneNumber,
            "email":email,
        }

        contacts.push(newContact);
        let tableRow = '<tr>\n' +
            '<td class=fName contenteditable="true">'+ newContact.firstName + '</td>\n' +
            '<td class=lName contenteditable="true">'+ newContact.lastName + '</td>\n' +
            '<td class=pNum contenteditable="true">'+ newContact.phoneNumber + '</td>\n' +
            '<td class=email contenteditable="true">'+ newContact.email + '</td>\n' +
            '<td><button onclick=editContact('+ newContact.id +') id="editContact' + newContact.id +'"><i class="fa-solid fa-pen"></i></button></td>\n' +
            '<td><button onclick=deleteContact('+ newContact.id +') id="deleteContact' + newContact.id +'"><i class="fa-solid fa-trash-can"></i></button></td>\n' +
            'tr'

        $('#contacts').append(tableRow);
        localStorage.removeItem('Contacts');
        window.localStorage.setItem('Contacts', JSON.stringify(contacts));
        clean_inputs();
    }
}

function searchContact(){
    let contacts = JSON.parse(window.localStorage.getItem('Contacts'));
    let flag = false;
    let contactName = $('#fname-search').val();
    let trResults = ''

    $('#search-contact table').remove();
    $('#search-contact .div-contacts').remove();
    
    for (let key in contacts) {
        let obj = contacts[key];
        if(obj.firstName.includes(contactName)) {
            if (flag == false) {
                let table = '<div class="div-contacts">\n' +
                '<table id="contacts">\n' +
                    '<h1>contacts</h1>\n' +
                    '<tr id="contacts-fields">\n' +
                        '<th width="300">First Name</th>\n' +
                        '<th width="300">Last Name</th>\n' +
                        '<th width="200">Phone No</th>\n' +
                        '<th width="400">E-mail</th>\n' +
                        '<th width="50">Edit</th>\n' +
                        '<th width="50">Delete</th>\n' +
                    '</tr>\n' +
        
                '</table>\n' +
                '</div>';
                $('#search-contact').append(table);
            }

            trResults +='<tr id="search-contact'+ obj.id + '">\n' +
                        '<td class=fName contenteditable="true">'+ obj.firstName +'</td>\n' +
                        '<td class=lName contenteditable="true">'+ obj.lastName +'</td>\n' +
                        '<td class=pNum contenteditable="true">'+ obj.phoneNumber +'</td>\n' +
                        '<td class=email contenteditable="true">'+ obj.email +'</td>\n' +
                        '<td><button onclick=editContact('+ obj.id +') id="editSearchContact' + obj.id +'"><i class="fa-solid fa-pen"></i></button></td>\n' +
                        '<td><button onclick=deleteContact('+ obj.id +') id="deleteSearchContact' + obj.id +'"><i class="fa-solid fa-trash-can"></i></button></td>\n' +
                    '</tr>\n';
            flag = true;
        }
    }

    if(flag) {
        $('#search-contact p').remove();
        $('#search-contact table#contacts').append(trResults);
    } else {
        $('#search-contact table').remove();
        $('#search-contact .div-contacts').remove();

        let p = $('#search-contact p').html();
        if(!p) {
            let paragraph = '<p>No such contact.</p>';
            $('#search-contact').append(paragraph);
        }
    }
}
